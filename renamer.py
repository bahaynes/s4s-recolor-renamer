

from os import walk
from os.path import isfile, join
from string import sort, partition, rpartition
from shutil import copy
import argparse
import pyhash

parser = argparse.ArgumentParser()
arguments = parser.parse_args()

hasher = pyhash.fnv1a_64()

# stolen from https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
def get_filepaths(directory):
    """
    This function will generate the file names in a directory
    tree by walking the tree either top-down or bottom-up. For each
    directory in the tree rooted at directory top (including top itself),
    it yields a 3-tuple (dirpath, dirnames, filenames).
    """
    file_paths = []  # List which will store all of the full filepaths.

    # Walk the tree.
    for root, directories, files in os.walk(directory):
        for filename in files:
            # Join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)  # Add it to the list.

    return file_paths  # Self-explanatory.

def extract_cas_names(file_paths):
    """
    This function loops through the list "files" and extracts the swatch names
    of the items therein.
    """
    cas_names = [] # List which will store all of the cas names in order
    image_names = [] # List which will store all of the s4s image hashes
    recolor_names = [] # List which will store all of the recolor paths

    for path in file_paths:
        left_name, seperator, file_ext = path.basename().partition('.')
        hash_name, seperator, cas_name = left_name.rpartition('.')
        if file_ext == "CASPart":
            cas_names.append((path, cas_name, hasher(cas_name)))
        if cas_name == "RLE2Image":
            mixed_hash, seperator, part_id = hash_name.partition('!')
            part_type, seperator, part_group = mixed_hash.partition('!')
            image_names.append((path, part_type, part_group)))
        if cas_name == "recolor":
            recolor_names.append((path,hash_name))

    # Sort the names by alphabetical
    # cas_names.sort()
    # Return the lists
    return cas_names, image_names, recolor_names

def link_cas_files(file_path,new_name):
    copy(file_path,new_name)

if __name__ == '__main__':
    cas_names, image_names, recolor_names = extract_cas_names(get_filepaths(cwd))
    for file_path in cas_names:
        rename_file(file_path)